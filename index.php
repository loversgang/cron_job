<?php

error_reporting(E_ERROR | E_PARSE);
ini_set('memory_limit', '2048M');
define('WP_MEMORY_LIMIT', '2048M');
set_time_limit(0);
ignore_user_abort(true);
$con = mysqli_connect("localhost", "root", "cwebco", "cron_job");
// Check connection
if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}
include_once('function/function.php');
include_once('classes/html_dom.php');
include_once('classes/simple_html_dom.php');
include_once('comics_store/impulsecreations.php');
error_reporting(E_ERROR | E_PARSE);
global $wpdb;

$group = isset($_GET['group']) ? $_GET['group'] : '0';

$ScriptRun = array();
$tableRun = '';


##########################################
# Data For 'wp_comics_store' Table Start #
##########################################

if ($group == '1'):
    /* Add Script For Site "www.midtowncomics.com" */
    ## 1 ##
    ///////////// $ScriptRun[]="www.midtowncomics.com";
    include_once('comics_store/impulsecreations.php'); //(Not Working As object)
    //include_once('comics_store/midtowncomics.php'); //(Not Working As object)

    /* Add Script For Site "www.scottscomics.com" */
    ## 2 ##
    # $ScriptRun[]="www.scottscomics.com";
    #include_once('comics_store/scottscomics.php');		

    /* Add Script For Site "www.bonanza.com" */
    ## 3 ##
    # $ScriptRun[]="www.bonanza.com";
    #include_once('comics_store/bonanza.php');		

    /* Add Script For Site "www.inter-comics.com" */
    ## 4 ##
    # $ScriptRun[]="www.nter-comics.com";
    #include_once('comics_store/inter-comics.php');		

    /* Add Script For Site "www.forbiddenplanet.com" */
    ## 5 ##
    # $ScriptRun[]="www.forbiddenplanet.com";
    #include_once('comics_store/forbiddenplanet.php');		

    /* Add Script For Site "www.tfaw.com" */
    ## 6 ##
    #$ScriptRun[]="www.tfaw.com";
    #include_once('comics_store/tfaw.php');	

    /* Add Script For Site "www.newkadia.com" */
    ## 7 ##
    #$ScriptRun[]="www.newkadia.com";
    #include_once('comics_store/newkadia.php');	

    /* Add Script For Site "www.grahamcrackers.com" */
    ## 8 ##
    #$ScriptRun[]="www.grahamcrackers.com";
    #include_once('comicbook/grahamcrackers.php');	(No Products On Site)	

    /* Add Script For Site "dreamlandcomics.com" */
    ## 9 ##
    #$ScriptRun[]="www.dreamlandcomics.com";
    #include_once('comics_store/dreamlandcomics.php');		

    /* Add Script For Site "www.metropoliscomics.com" */
    ## 10 ##
    #$ScriptRun[]="www.metropoliscomics.com";
    #include_once('comics_store/metropoliscomics.php');	

    /* Add Script For Site "www.atomicavenue.com" */
    ## 11 ##
    #$ScriptRun[]="http://atomicavenue.com";
    #include_once('comics_store/atomicavenue.php');	 	

    /* Add Script For Site "www.comiccollectorlive.com" */
    ## 12 ##
    #$ScriptRun[]="www.comiccollectorlive.com";
    #include_once('comics_store/comiccollectorlivecomiccollectorlive.php');	  (using session and ajax)
    ## 13 ##
    #$ScriptRun[]="www.comiclink.com";
    #include_once('comics_store/comiclink.php');

    /* Add Script For Site "www.cragslist.com " */
    ## 14 ##
    #$ScriptRun[]="www.cragslist.com";
    ///////////include_once('comics_store/cragslist.php');	 //(Not Made For Multiple Cities)			

    /* Add Script For Site "www.mycomicshop.com" */
    ## 15 ##
    #$ScriptRun[]="www.mycomicshop.com";
    #include_once('comics_store/mycomicshop.php');			

    /* Add Script For Site "www.impulsecreations.com" */
    ## 16 ##
    #$ScriptRun[]="www.impulsecreations.com";
    //////////////////////include_once('comics_store/impulsecreations.php'); (Not Made Too large Category)				

    /* Add Script For Site "www.westfieldcomics.com" */
    ## 17 ##
    #$ScriptRun[]="www.westfieldcomics.com";
    #include_once('comics_store/westfieldcomics.php'); //(Not Made)	

    /* Add Script For Site "www.g-mart.com" */
    ## 18 ##
    #$ScriptRun[]="www.g-mart.com";
    #include_once('comics_store/g-mart.php'); 			

    $tableRun = 'wp_comics_store';
endif;



if (!empty($tableRun)):
    echo "Data Imported For Table '" . $tableRun . "'";
    echo "<br/>";
    echo "From These sites :<br/> <strong>" . implode('<br/>', $ScriptRun) . "</strong>";
endif;

echo '<br/> Cron Done';
exit;
?>