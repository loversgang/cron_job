<?php

// Author: Balbinder Singh
// Products Scrapping From http://www.impulsecreations.net
function pr($e) {
    echo "<pre>";
    print_r($e);
    echo "</pre>";
}

function getData($url) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_HEADER, FALSE);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_REFERER, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    $str = curl_exec($curl);
    curl_close($curl);
    $html_base = new simple_html_dom();
    return $html_base->load($str);
}

$date = date('d-m-Y');

/* Fetch Data From http://impulsecreations.com/ */
$SiteName = "http://www.impulsecreations.net";
$base = "http://www.impulsecreations.net/catalog/comic-books-c-21.html";
//get all categories data
$cat_data = getData($base);

// Make Categories Array
$data_array = array();
foreach ($cat_data->find('.smallText a') as $element) {
    $title = strip_tags($element->innertext);
    $url = $element->href;
    if (!empty($title)) {
        $data_array[] = $url;
    }
}
$cat_data->clear();
unset($cat_data);
// Fetch Products Data
foreach ($data_array as $cat_url) {
    $products_url = $cat_url;
    $product_data = getData($products_url);
    $total_products = $product_data->find('.smallText b')[2]->innertext;
    $total_product_count = intval($total_products);
    $total_pages = ceil($total_product_count / 40);
    for ($i = 1; $i <= $total_pages; $i++) {
        $single_product_url = $products_url . '?page=' . $i;
        $product_data_html = getData($single_product_url);
        foreach ($product_data_html->find('table.main tr') as $p_key => $p_element) {
            if (!$p_key) {
                continue;
            }
            //get link
            $p_url_data = $p_element->find('td.productListing-data[align="center"] a', 0);
            $p_url = $p_url_data->href;

            //get image link
            $img_elem = $p_element->find('td.productListing-data[align="center"] a img', 0);
            if (!$img_elem) {
                $p_img_url = '';
            } else {
                $p_img_url = '<img width="200" src="http://www.impulsecreations.net/catalog/' . $img_elem->src . '"/>';
            }

            //get price
            $product_title = $p_element->find('td.productListing-data a', 1);
            $p_title = strip_tags($product_title->innertext);

            //get price
            $price_elem = $p_element->find('td.productListing-data', 3);
            if ($p_element->find("td.productListing-data .productSpecialPrice")) {
                $price_elem = $price_elem->find(".productSpecialPrice", 0);
            }
            $p_price = strip_tags($price_elem->innertext);
            // Save Into Database
            mysqli_query($con, "INSERT INTO wp_comics_store (title,site_url,product_url,url,img,price,date,status) VALUES ('$p_title','$SiteName','$p_url','$p_url','$p_img_url','$p_price','$date','1')");
        }
    }
}
echo "Cron Jobe Done!";
