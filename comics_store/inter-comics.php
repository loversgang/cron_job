<?php	
	/* Fetch Data From https://www.inter-comics.com */
	$SiteName="https://www.inter-comics.com";
	
		###############################
		/*  Get Collection From Shop */
		###############################
		
		$TotalPages='294';
				
		for($i=1;$i<=$TotalPages;$i++)
		{
			$html = file_get_html('https://www.inter-comics.com/shop/collection/catalog/page/'.$i);
			if(is_object($html))
			{	
				$records=array();
				foreach($html->find('.cover-normal > a') as $element)
				{
					$records["site_url"][] =$element->href;
				}   
				foreach($html->find('.cover-normal img') as  $element)
				{ 
					$full = $element->src;
					$records["image_src"][] =  '<img width="200" src="'.$full.'"/>';
				} 
				foreach($html->find('.now') as $element)
				{
				 $records["price"][] =$element->plaintext;
				}  

				foreach($html->find('.cover-normal > a > h2') as $element)
				{
				  $records["product_title"][] =$element->plaintext;
				  
				}  
				  foreach($html->find('.price') as $element)
				{
					$records["price"][] =$element->plaintext;				  
				}  
				
				$a=0;
				foreach($records["site_url"] as $key=>$val)
				{
					$da = mysql_query("SELECT * FROM `wp_comics_store` where `title`='".$records["product_title"][$key]."' AND `site_url`='".$SiteName."'");
					
					while($info = mysql_fetch_array($da)) 
					{ 
						$a++;
					} 
					
					if($a==0)
					{  
					$b=date("d-m-Y");
					$text_query="INSERT INTO `wp_comics_store` (`title`,`url`,`img`,`price`,`date`,`status`,`site_url`,`product_url`) VALUES('".$records["product_title"][$key]."','".$val."','".$records["image_src"][$key]."','".$records["price"][$key]."','".$b."','enable','".$SiteName."','".$val."')";
						
					$qry=mysql_query($text_query);
						if($qry)
						{
							
						}	
					}
				}
			}				
		}

	 
		#######################
		/*  Get New Release  */
		#######################
		
		$TotalPages='2';
				
		for($i=1;$i<=$TotalPages;$i++)
		{	 
			$html = file_get_html('https://www.inter-comics.com/shop/collection/new-release-this-week/page/'.$i);
			if(is_object($html))
			{	 
				foreach($html->find('.cover-normal > a') as $element)
				{
					$records["site_url"][] =$element->href;
				}   
				foreach($html->find('.cover-normal img') as  $element)
				{ 
					 $full = $element->src;
					 $records["image_src"][] =  '<img width="200" src="'.$full.'"/>';
				} 
				foreach($html->find('.now') as $element)
				{
					$records["price"][] =$element->plaintext;
				}  

				foreach($html->find('.cover-normal > a > h2') as $element)
				{
					$records["product_title"][] =$element->plaintext;
				}  
				foreach($html->find('.price') as $element)
				{
					$records["price"][] =$element->plaintext;
				}  

				$a=0;
				foreach($records["site_url"] as $key=>$val)
				{
					$da = mysql_query("SELECT * FROM `wp_comics_store` where `title`='".$records["product_title"][$key]."' AND `site_url`='".$SiteName."'");
					while($info = mysql_fetch_array($da)) 
					{ 
						$a++;
					} 

					if($a==0)
					{  
						$b=date("d-m-Y");
						$text_query="INSERT INTO `wp_comics_store` (`title`,`url`,`img`,`price`,`date`,`status`,`site_url`,`product_url`) VALUES('".$records["product_title"][$key]."','".$val."','".$records["image_src"][$key]."','".$records["price"][$key]."','".$b."','enable','".$SiteName."','".$val."')";
						$qry=mysql_query($text_query);
						if($qry)
						{
							
						}	
					}
				}	 
			}
		}
		
	
		##########################	
		/*  Get New Release DC  */
		##########################
		
		$html = file_get_html('https://www.inter-comics.com/shop/collection/new-release-this-week-dc');
		if(is_object($html))
		{	
			foreach($html->find('.cover-normal > a') as $element)
			{
				$records["site_url"][] =$element->href;
			}   
			foreach($html->find('.cover-normal img') as  $element)
			{ 
				$full = $element->src;
				$records["image_src"][] =  '<img width="200" src="'.$full.'"/>';
			} 
			foreach($html->find('.now') as $element)
			{
				$records["price"][] =$element->plaintext;
			}  

			foreach($html->find('.cover-normal > a > h2') as $element)
			{
				$records["product_title"][] =$element->plaintext;
			}  
			foreach($html->find('.price') as $element)
			{
				$records["price"][] =$element->plaintext;
			}  


			$a=0;
			foreach($records["site_url"] as $key=>$val)
			{

				$da = mysql_query("SELECT * FROM `wp_comics_store` where `title`='".$records["product_title"][$key]."' AND `site_url`='".$SiteName."'");
				while($info = mysql_fetch_array($da)) 
				{ 
							$a++;
				} 

				if($a==0)
				{  
					$b=date("d-m-Y");
					$text_query="INSERT INTO `wp_comics_store` (`title`,`url`,`img`,`price`,`date`,`status`,`site_url`,`product_url`) VALUES('".$records["product_title"][$key]."','".$val."','".$records["image_src"][$key]."','".$records["price"][$key]."','".$b."','enable','".$SiteName."','".$val."')";
					
					$qry=mysql_query($text_query);
					if($qry)
					{
						
					}	
				}
			} 
		}
		
		
		###########################	
		/*  Get On Sale Product  */
		###########################		
		
		$TotalPages='5';
		
		for($i=1;$i<=$TotalPages;$i++)
		{	 
			$html = file_get_html('https://www.inter-comics.com/shop/collection/onsale/'.$i);
			if(is_object($html))
			{
				$records=array();

				foreach($html->find('.cover-normal > a') as $element)
				{
					$records["site_url"][] =$element->href;
				}   
				foreach($html->find('.cover-normal img') as  $element)
				{ 
					$full = $element->src;
					$records["image_src"][] =  '<img width="200" src="'.$full.'"/>';
				} 
				foreach($html->find('.now') as $element)
				{
					$records["price"][] =$element->plaintext;
				}  

				foreach($html->find('.cover-normal > a > h2') as $element)
				{
					$records["product_title"][] =$element->plaintext;
				}  
				foreach($html->find('.price') as $element)
				{
					$records["price"][] =$element->plaintext;
				}  
				
				$a=0;
				foreach($records["site_url"] as $key=>$val)
				{
					$da = mysql_query("SELECT * FROM `wp_comics_store` where `title`='".$records["product_title"][$key]."' AND `site_url`='".$SiteName."'");
					while($info = mysql_fetch_array($da)) 
					{ 
						$a++;
					} 

					if($a==0)
					{  
						$b=date("d-m-Y");
						$text_query="INSERT INTO `wp_comics_store` (`title`,`url`,`img`,`price`,`date`,`status`,`site_url`,`product_url`) VALUES('".$records["product_title"][$key]."','".$val."','".$records["image_src"][$key]."','".$records["price"][$key]."','".$b."','enable','".$SiteName."','".$val."')";
						
						$qry=mysql_query($text_query);
						if($qry)
						{
						
						}	
					}
				} 
			}
		}	
		
		
		
		##############################	
		/*  Get onsale-independent  */
		##############################
		
		$html = file_get_html('https://www.inter-comics.com/shop/collection/onsale-independent');
		if(is_object($html))
		{	
			$records=array();
			foreach($html->find('.cover-normal > a') as $element)
			{
				$records["site_url"][] =$element->href;
			}   
			foreach($html->find('.cover-normal img') as  $element)
			{ 
				$full = $element->src;
				$records["image_src"][] =  '<img width="200" src="'.$full.'"/>';
			} 
			foreach($html->find('.now') as $element)
			{
				$records["price"][] =$element->plaintext;
			}  

			foreach($html->find('.cover-normal > a > h2') as $element)
			{
				$records["product_title"][] =$element->plaintext;
			}  
			foreach($html->find('.price') as $element)
			{
				$records["price"][] =$element->plaintext;
			}  

			$a=0;
			foreach($records["site_url"] as $key=>$val)
			{
				$da = mysql_query("SELECT * FROM `wp_comics_store` where `title`='".$records["product_title"][$key]."' AND `site_url`='".$SiteName."'");
				while($info = mysql_fetch_array($da)) 
				{ 
					$a++;
				} 

				if($a==0)
				{  
					$b=date("d-m-Y");
					$text_query="INSERT INTO `wp_comics_store` (`title`,`url`,`img`,`price`,`date`,`status`,`site_url`,`product_url`) VALUES('".$records["product_title"][$key]."','".$val."','".$records["image_src"][$key]."','".$records["price"][$key]."','".$b."','enable','".$SiteName."','".$val."')";
					
					$qry=mysql_query($text_query);
					if($qry)
					{
				
					}	
				}
			} 
		}
?>